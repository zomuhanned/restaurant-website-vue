import HomePage from './components/Home.vue'
import SignUp from './components/SignUp.vue'
import LogIn from './components/Login.vue'
import AddRestaurant from './components/Restaurant/Add.vue'
import EditRestaurant from './components/Restaurant/Edit.vue'
import { createRouter, createWebHistory } from 'vue-router'

const routes = [
    {
        name: 'Home',
        component: HomePage,
        path: '/'
    },
    {
        name: 'SignUp',
        component: SignUp,
        path: '/sign-up'
    },
    {
        name: 'LogIn',
        component: LogIn,
        path: '/login'
    },
    {
        name: 'AddRestaurant',
        component: AddRestaurant,
        path: '/restaurant/add'
    },
    {
        name: 'EditRestaurant',
        component: EditRestaurant,
        path: '/restaurant/edit/:id'
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
